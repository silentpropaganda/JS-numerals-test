// The neccessary digits for the program
var single_digits = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine',
			'ten', 'eleven', 'twelve',
			'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

var tens = ['twenty', 'thirty', 'fourty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

// The processing methods in descending order
var methods = [
{'scale':'billion','divider':1000000000},
{'scale':'million','divider':1000000},
{'scale':'thousand','divider':1000},
{'scale':'hundred','divider':100}
]

// The main function that we want to call from outside
function converter (input) {	
	if (isNaN(input)){
		return 'Sorry you have to specify a number'
	}
	else {
	return convert_high(input).join(" ")
	}
}
// Starting to convert from high to low
function convert_high (input) {	
	var part_result;
	var result = [];
	// Going through the methods
	for (i in methods) {
		// if the divider is lower than the input, we divide the input
		if (methods[i]['divider'] <= input){			
			part_result = Math.floor(input / methods[i]['divider']);
			// We cut the part from the number that we alredy processed
			input -= part_result * methods[i]['divider'];		
			// If the part is bigger than 99 we need to add a + hundred in the string
			if (part_result > 99) {
				result.push(single_digits[Math.floor(part_result / 100)]+' hundred');
				// And we take the rest to the next process
				part_result %= 100;
			}
			// If the remainder is not 0 we add the rest 2 (or 1) digits
			part_result > 0 ? result.push(convert_double_digits(part_result)+' '+methods[i]['scale']) : result.push(methods[i]['scale']);						
		}		
	}
	// When we out of methods, we only have a number that lower than 100 because we alredy deleted all bigger numbers
	// so we just need the double digits
	input > 0 ? result.push(convert_double_digits(input)) : false;
	// If the number is longer than 1 part in the array, it adds an and
	result.length > 1 ? result.splice(-1,0, "and") : false;
	return result;	
}
// Converting single digits.
function convert_single_digit (input) {	
		return(single_digits[input]);	
}
// Converting everything that lower than 100, if it's lower than 20, the convert_single_digit gona process it
function convert_double_digits (input) {
	var result
	if (input < 20 && input != 0) {
		return convert_single_digit(input);
	}
	else {
		result = tens[Math.floor((input/10)-2)]
		input%10 != 0 ? result += '-'+single_digits[input%10] : false;
	}
	return result;
}